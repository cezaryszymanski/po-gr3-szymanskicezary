package pl.edu.uwm.wmii.szymanskicezary.z12;

import java.util.HashSet;
import java.util.Scanner;

public class zad7 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Podaj liczbę: ");
        int n= sc.nextInt();
        HashSet<Integer> primes=new HashSet<>();
        for(int i=2;i<=n;i++){
            primes.add(i);
        }
        for(int i=2;i<=Math.sqrt(n);i++){
            for(int j=i+1;j<=n;j++){
                if(j%i==0){
                    primes.remove(j);
                }
            }
        }
        System.out.println(primes);
    }
}
