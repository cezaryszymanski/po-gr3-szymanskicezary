package pl.edu.uwm.wmii.szymanskicezary.z12;

import java.util.LinkedList;

public class zad3 {

    public static void main(String[] args) {
        LinkedList<String> pracownicy=new LinkedList<>();
        pracownicy.add("Nowak");
        pracownicy.add("Szymański");
        pracownicy.add("Kowalski");
        pracownicy.add("Kozłowski");
        pracownicy.add("Borecki");
        pracownicy.add("Tetmer");
        pracownicy.add("Kononowicz");
        System.out.println(pracownicy);
        odwroc(pracownicy);
        System.out.println(pracownicy);

    }
    public static void odwroc(LinkedList<String> lista){
        LinkedList<String> lista2=new LinkedList<>();
        for(int i=lista.size()-1;i>=0;i--){
            lista2.add(lista.get(i));
        }
        lista.clear();
        lista.addAll(lista2);
    }

}

