package pl.edu.uwm.wmii.szymanskicezary.z12;

import java.util.Stack;

public class zad5 {
    public static void main(String[] args) {
        String zdanie="Ala ma kota.";
        Stack<String> stos=new Stack<>();
        String[] arr = zdanie.split(" ");
        int j=0;
        for(String ss:arr){
            if (ss.contains(".")){
                stos.push(ss.substring(0,1).toUpperCase()+ss.substring(1,ss.length()-1));
            }
            else if (j==0){
                stos.push((ss.toLowerCase())+".");
                j++;
            }
            else{
                stos.push(ss);
            }
        }
        while (!stos.empty()) {
            System.out.print(stos.pop() + " ");
        }
    }
}

