package pl.edu.uwm.wmii.szymanskicezary.z12;

import java.util.LinkedList;

public class zad2 {

    public static void main(String[] args) {
        LinkedList<String> pracownicy=new LinkedList<>();
        pracownicy.add("Nowak");
        pracownicy.add("Szymański");
        pracownicy.add("Kowalski");
        pracownicy.add("Kozłowski");
        pracownicy.add("Borecki");
        pracownicy.add("Tetmer");
        pracownicy.add("Kononowicz");
        System.out.println(pracownicy);
        redukuj(pracownicy,2);
        System.out.println(pracownicy);

    }
    public static <T> void redukuj(LinkedList<T> pracownicy, int n){
        for(int i=n-1;i<pracownicy.size();i=i+n){
            pracownicy.remove(i);
            i--;
        }
    }

}

