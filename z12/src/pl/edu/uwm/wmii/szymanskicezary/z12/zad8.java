package pl.edu.uwm.wmii.szymanskicezary.z12;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;

public class zad8 {
    public static void main(String[] args) {
        LinkedList<String> pracownicy=new LinkedList<>();
        pracownicy.add("Nowak");
        pracownicy.add("Szymański");
        pracownicy.add("Kowalski");
        pracownicy.add("Kononowicz");
        HashSet<Integer> primes=new HashSet<>();
        primes.add(1);
        primes.add(2);
        primes.add(3);
        primes.add(4);
        print(pracownicy);
        System.out.println("");
        print(primes);
    }
    public static <T extends Iterable> void print(T obj){
        Iterator iter= obj.iterator();
        System.out.print(iter.next());
        while (iter.hasNext()){
            System.out.print(","+iter.next());
        }
    }
}
