package pl.edu.uwm.wmii.szymanskicezary.z12;

import java.util.Scanner;
import java.util.Stack;

public class zad6 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Podaj nieujemną liczbę całkowitą");
        int liczba= sc.nextInt();
        Stack stos=new Stack();
        while (liczba!=0){
            stos.push(liczba%10);
            liczba=liczba/10;
        }
        while (!stos.empty()){
            System.out.print(stos.pop()+" ");
        }
    }
}

