package pl.edu.uwm.wmii.szymanskicezary.laboratorium06;

import pl.imiajd.szymanski.Adres;

public class zad3 {
    public static void main(String[] args){
        Adres adres1=new Adres("Bananowa","22","4");
        adres1.pokaz();
        System.out.println();
        Adres adres2=new Adres("Czekoladowa","18","7","Olsztyn","10-008");
        adres2.pokaz();
        System.out.println();
        Adres adres3=new Adres("Kolorowa","14","2","Olsztyn","10-011");
        adres3.pokaz();
        System.out.println();
        System.out.println(adres2.przed(adres3));
        System.out.println(adres3.przed(adres2));
    }
}
