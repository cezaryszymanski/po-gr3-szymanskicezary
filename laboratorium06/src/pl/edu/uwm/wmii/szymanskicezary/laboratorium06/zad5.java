package pl.edu.uwm.wmii.szymanskicezary.laboratorium06;

import pl.imiajd.szymanski.Osoba;
import pl.imiajd.szymanski.Student;
import pl.imiajd.szymanski.Nauczyciel;

public class zad5 {
    public static void main(String[] args){
        Osoba osoba1=new Osoba("Karolak","1999");
        Student student1=new Student("Baranowski","2000","Informatyka");
        Nauczyciel nauczyciel1=new Nauczyciel("Malinowski","1972","8000");
        System.out.println("Nazwisko osoby:"+osoba1.getNazwisko());
        System.out.println("Rok urodzenia osoby:"+osoba1.getRokUrodzenia());
        System.out.println();
        System.out.println("Nazwisko studenta:"+student1.getNazwisko());
        System.out.println("Rok urodzenia studenta:"+student1.getRokUrodzenia());
        System.out.println("Kierunek studenta:"+student1.getKierunek());
        System.out.println();
        System.out.println("Nazwisko nauczyciela:"+nauczyciel1.getNazwisko());
        System.out.println("Rok urodzenia nauczyciela:"+nauczyciel1.getRokUrodzenia());
        System.out.println("Pensja nauczyciela:"+nauczyciel1.getPensja());
        System.out.println();
        System.out.println(osoba1.toString());
        System.out.println();
        System.out.println(student1.toString());
        System.out.println();
        System.out.println(nauczyciel1.toString());
    }
}
