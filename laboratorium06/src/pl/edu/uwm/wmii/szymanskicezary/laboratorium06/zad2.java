package pl.edu.uwm.wmii.szymanskicezary.laboratorium06;

public class zad2 {
    public static void main(String[] args){
        Adres adres1=new Adres("Bananowa","22","4");
        adres1.pokaz();
        System.out.println();
        Adres adres2=new Adres("Czekoladowa","18","7","Olsztyn","10-008");
        adres2.pokaz();
        System.out.println();
        Adres adres3=new Adres("Kolorowa","14","2","Olsztyn","10-011");
        adres3.pokaz();
        System.out.println();
        System.out.println(adres2.przed(adres3));
        System.out.println(adres3.przed(adres2));
    }
}
class Adres{
    private String ulica,numer_domu,numer_mieszkania,miasto,kod_pocztowy;
    public Adres(String ulica,String numer_domu,String numer_mieszkania){
        this.ulica=ulica;
        this.numer_domu=numer_domu;
        this.numer_mieszkania=numer_mieszkania;
    }
    public Adres(String ulica,String numer_domu,String numer_mieszkania,String miasto,String kod_pocztowy){
        this.ulica=ulica;
        this.numer_domu=numer_domu;
        this.numer_mieszkania=numer_mieszkania;
        this.miasto=miasto;
        this.kod_pocztowy=kod_pocztowy;
    }
    public void pokaz(){
        if(kod_pocztowy!=null&&numer_mieszkania!=null){
            System.out.println(kod_pocztowy+" "+miasto);
        }
        System.out.println(ulica+" "+numer_domu+'/'+numer_mieszkania);
    }
    public boolean przed(Adres adres){
        for(int i=0;i<kod_pocztowy.length();i++)
        {
            if(kod_pocztowy.charAt(i)<adres.kod_pocztowy.charAt(i))
                return true;
            if(kod_pocztowy.charAt(i)>adres.kod_pocztowy.charAt(i))
                return false;
        }
        return false;
    }
}
