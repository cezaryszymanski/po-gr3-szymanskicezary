package pl.edu.uwm.wmii.szymanskicezary.laboratorium06;

public class zad4 {
    public static void main(String[] args){
        Osoba osoba1=new Osoba("Karolak","1999");
        Student student1=new Student("Baranowski","2000","Informatyka");
        Nauczyciel nauczyciel1=new Nauczyciel("Malinowski","1972","8000");
        System.out.println("Nazwisko osoby:"+osoba1.getNazwisko());
        System.out.println("Rok urodzenia osoby:"+osoba1.getRokUrodzenia());
        System.out.println();
        System.out.println("Nazwisko studenta:"+student1.getNazwisko());
        System.out.println("Rok urodzenia studenta:"+student1.getRokUrodzenia());
        System.out.println("Kierunek studenta:"+student1.getKierunek());
        System.out.println();
        System.out.println("Nazwisko nauczyciela:"+nauczyciel1.getNazwisko());
        System.out.println("Rok urodzenia nauczyciela:"+nauczyciel1.getRokUrodzenia());
        System.out.println("Pensja nauczyciela:"+nauczyciel1.getPensja());
        System.out.println();
        System.out.println(osoba1.toString());
        System.out.println();
        System.out.println(student1.toString());
        System.out.println();
        System.out.println(nauczyciel1.toString());
    }
}
class Osoba{
    private String nazwisko,rokUrodzenia;
    public Osoba(String nazwisko,String rokUrodzenia){
        this.nazwisko=nazwisko;
        this.rokUrodzenia=rokUrodzenia;
    }
    @Override
    public String toString(){
        return "nazwisko:"+nazwisko+", rok urodzenia:"+rokUrodzenia;
    }
    public String getNazwisko(){
        return nazwisko;
    }
    public String getRokUrodzenia(){
        return rokUrodzenia;
    }
}
class Student extends Osoba{
    private String kierunek;
    public Student(String nazwisko,String rokUrodzenia,String kierunek){
        super(nazwisko, rokUrodzenia);
        this.kierunek=kierunek;
    }
    @Override
    public String toString(){
        return super.toString()+", kierunek:"+kierunek;
    }

    public String getKierunek() {
        return kierunek;
    }
}
class Nauczyciel extends Osoba{
    private String pensja;
    public Nauczyciel(String nazwisko,String rokUrodzenia, String pensja){
        super(nazwisko, rokUrodzenia);
        this.pensja=pensja;
    }
    @Override
    public String toString(){
        return super.toString()+", pensja:"+pensja;
    }

    public String getPensja() {
        return pensja;
    }
}
