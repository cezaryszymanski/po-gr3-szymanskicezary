package pl.edu.uwm.wmii.szymanskicezary.laboratorium06;

import java.awt.Rectangle;

public class zad7{
    public static void main(String[] args){
        BetterRectanglev2 prostokat=new BetterRectanglev2(8,4,2,1);
        System.out.println("height:"+prostokat.height);
        System.out.println("width:"+prostokat.width);
        System.out.println("x:"+prostokat.x);
        System.out.println("y:"+prostokat.y);
        System.out.println("Obwód:"+prostokat.getPerimeter());
        System.out.println("Pole:"+prostokat.GetArea());
    }
}
class BetterRectanglev2 extends Rectangle{
    public BetterRectanglev2(int height,int width,int x,int y){
        super(x, y, width, height);
    }
    public int getPerimeter(){
        return 2*height+2*width;
    }
    public int GetArea(){
        return height*width;
    }
}
