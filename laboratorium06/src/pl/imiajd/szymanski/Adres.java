package pl.imiajd.szymanski;

public class Adres {
    private String ulica,numer_domu,numer_mieszkania,miasto,kod_pocztowy;
    public Adres(String ulica,String numer_domu,String numer_mieszkania){
        this.ulica=ulica;
        this.numer_domu=numer_domu;
        this.numer_mieszkania=numer_mieszkania;
    }
    public Adres(String ulica,String numer_domu,String numer_mieszkania,String miasto,String kod_pocztowy){
        this.ulica=ulica;
        this.numer_domu=numer_domu;
        this.numer_mieszkania=numer_mieszkania;
        this.miasto=miasto;
        this.kod_pocztowy=kod_pocztowy;
    }
    public void pokaz(){
        if(kod_pocztowy!=null&&numer_mieszkania!=null){
            System.out.println(kod_pocztowy+" "+miasto);
        }
        System.out.println(ulica+" "+numer_domu+'/'+numer_mieszkania);
    }
    public boolean przed(Adres adres){
        for(int i=0;i<kod_pocztowy.length();i++)
        {
            if(kod_pocztowy.charAt(i)<adres.kod_pocztowy.charAt(i))
                return true;
            if(kod_pocztowy.charAt(i)>adres.kod_pocztowy.charAt(i))
                return false;
        }
        return false;
    }
}
