package pl.imiajd.szymanski;

import java.awt.Rectangle;

public class BetterRectangle extends Rectangle {
    public BetterRectangle(int height,int width,int x,int y){
        super.setLocation(x,y);
        super.setSize(width,height);
    }
    public int getPerimeter(){
        return 2*height+2*width;
    }
    public int GetArea(){
        return height*width;
    }
}
