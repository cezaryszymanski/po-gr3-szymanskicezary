package pl.imiajd.szymanski;

public class Student extends Osoba {
    private String kierunek;
    public Student(String nazwisko,String rokUrodzenia,String kierunek){
        super(nazwisko, rokUrodzenia);
        this.kierunek=kierunek;
    }
    @Override
    public String toString(){
        return super.toString()+", kierunek:"+kierunek;
    }

    public String getKierunek() {
        return kierunek;
    }
}
