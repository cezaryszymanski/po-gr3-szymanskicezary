package pl.imiajd.szymanski;

public class Nauczyciel extends Osoba {
    private String pensja;
    public Nauczyciel(String nazwisko,String rokUrodzenia, String pensja){
        super(nazwisko, rokUrodzenia);
        this.pensja=pensja;
    }
    @Override
    public String toString(){
        return super.toString()+", pensja:"+pensja;
    }

    public String getPensja() {
        return pensja;
    }
}
