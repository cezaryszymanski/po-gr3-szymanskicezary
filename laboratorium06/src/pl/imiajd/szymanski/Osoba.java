package pl.imiajd.szymanski;

public class Osoba {
    private String nazwisko,rokUrodzenia;
    public Osoba(String nazwisko,String rokUrodzenia){
        this.nazwisko=nazwisko;
        this.rokUrodzenia=rokUrodzenia;
    }
    @Override
    public String toString(){
        return "nazwisko:"+nazwisko+", rok urodzenia:"+rokUrodzenia;
    }
    public String getNazwisko(){
        return nazwisko;
    }
    public String getRokUrodzenia(){
        return rokUrodzenia;
    }
}
