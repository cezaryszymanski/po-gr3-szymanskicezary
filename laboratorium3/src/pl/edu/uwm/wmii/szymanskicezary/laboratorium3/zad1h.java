package pl.edu.uwm.wmii.szymanskicezary.laboratorium3;

import java.util.Scanner;

public class zad1h {
    public static void main(String[] args){
        Scanner scan=new Scanner(System.in);
        System.out.print("Podaj napis: ");
        String napis=scan.nextLine();
        System.out.println(nice(napis,"|",2));
    }
    public static String nice(String str, String separator, int n){
        StringBuffer temp=new StringBuffer().append(str);
        for (int i=temp.length()-n;i>0;i=i-n){
            temp.insert(i,separator);
        }
        return temp.toString();
    }
}
