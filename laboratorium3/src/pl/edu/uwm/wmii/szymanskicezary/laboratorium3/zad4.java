package pl.edu.uwm.wmii.szymanskicezary.laboratorium3;

import java.math.BigInteger;

public class zad4 {
    public static void main(String[] args){
        int n=58;
        int rozmiar = n*n;
        BigInteger liczba=BigInteger.valueOf(0);
        if(n>0){
            liczba=BigInteger.valueOf(1);
        }
        for(int i=0;i<rozmiar;i++){
            liczba=liczba.multiply(BigInteger.valueOf(2));
        }
        System.out.println("Liczba ziarenek maku: "+liczba);
    }
}

