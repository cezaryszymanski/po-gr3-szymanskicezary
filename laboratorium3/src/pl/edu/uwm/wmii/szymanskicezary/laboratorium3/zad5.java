package pl.edu.uwm.wmii.szymanskicezary.laboratorium3;

import java.math.BigDecimal;

public class zad5 {
    public static void main(String[] args){
        BigDecimal k=BigDecimal.valueOf(1000);
        double p=1.05;
        int n=4;
        for(int i=0;i<n;i++){
            k=k.multiply(new BigDecimal(p));
        }
        System.out.println(k);
    }
}
