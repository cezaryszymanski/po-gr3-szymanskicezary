package pl.edu.uwm.wmii.szymanskicezary.laboratorium3;

import java.util.Scanner;
import java.util.Arrays;

public class zad1e {
    public static void main(String[] args){
        Scanner scan=new Scanner(System.in);
        System.out.print("Podaj napis: ");
        String napis=scan.nextLine();
        System.out.print("Podaj drugi napis: ");
        String napis2=scan.nextLine();
        System.out.println(Arrays.toString(where(napis, napis2)));
    }
    public static int[] where(String str, String subStr){
        int licznik=0;
        for(int i=0;i<str.length()-subStr.length()+1;i++){
            if(str.substring(i,i+subStr.length()).equals(subStr)){
                licznik+=1;
            }
        }
        int[] tab=new int[licznik];
        int j=0;
        for(int i=0;i<str.length()-subStr.length()+1;i++){
            if(str.substring(i,i+subStr.length()).equals(subStr)){
                tab[j]=i;
                j+=1;
            }
        }
        return tab;
    }
}
