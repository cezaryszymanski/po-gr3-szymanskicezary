package pl.edu.uwm.wmii.szymanskicezary.laboratorium3;

import java.util.Scanner;

public class zad1b {
    public static void main(String[] args){
        Scanner scan=new Scanner(System.in);
        System.out.print("Podaj napis: ");
        String napis=scan.nextLine();
        System.out.print("Podaj drugi napis: ");
        String napis2=scan.nextLine();
        System.out.println("Napis1 zawiera napis2 " +countSubStr(napis,napis2) +" razy");
    }
    public static int countSubStr(String str, String subStr){
        int licznik=0;
        for(int i=0;i<str.length()-subStr.length()+1;i++){
            if(str.substring(i,i+subStr.length()).equals(subStr)){
                licznik+=1;
            }
        }
        return licznik;
    }
}
