package pl.edu.uwm.wmii.szymanskicezary.laboratorium3;
import java.util.Scanner;
public class zad1a{

    public static void main(String[] args){
        Scanner scan=new Scanner(System.in);
        System.out.print("Podaj napis: ");
        String napis=scan.nextLine();
        System.out.print("Podaj znak do policzenia: ");
        char znak=scan.next().charAt(0);
        System.out.println("Znak wystepuje " +countChar(napis, znak) +" razy");
    }
    public static int countChar(String str, char c){
        int licznik=0;
        for(int i=0;i<str.length();i++){
            if(str.charAt(i)==c){
                licznik+=1;
            }
        }
        return licznik;
    }
}
