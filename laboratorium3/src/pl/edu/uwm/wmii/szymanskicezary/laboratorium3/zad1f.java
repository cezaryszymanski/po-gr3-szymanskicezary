package pl.edu.uwm.wmii.szymanskicezary.laboratorium3;

import java.util.Scanner;

public class zad1f {
    public static void main(String[] args){
        Scanner scan=new Scanner(System.in);
        System.out.print("Podaj napis: ");
        String napis=scan.nextLine();
        System.out.println(change(napis));
    }
    public static String change(String str){
        StringBuffer temp=new StringBuffer().append(str);
        for (int i=0;i<temp.length();i++){
            if(Character.isLowerCase(temp.charAt(i))){
                temp.setCharAt(i,Character.toUpperCase(temp.charAt(i)));
            }
            else if(Character.isUpperCase(temp.charAt(i))){
                temp.setCharAt(i,Character.toLowerCase(temp.charAt(i)));
            }
        }
        return temp.toString();
    }
}
