package pl.edu.uwm.wmii.szymanskicezary.laboratorium3;

import java.util.Scanner;

public class zad1c {
    public static void main(String[] args){
        Scanner scan=new Scanner(System.in);
        System.out.print("Podaj napis: ");
        String napis=scan.nextLine();
        System.out.println("Środek napisu: "+middle(napis));
    }
    public static String middle(String str){
        if(str.length()<=1){
            return "brak";
        }
        int half=str.length()/2;
        if(str.length()%2!=0){
            return str.substring(half,half+1);
        }else{
            return str.substring(half-1,half+1);
        }
    }
}
