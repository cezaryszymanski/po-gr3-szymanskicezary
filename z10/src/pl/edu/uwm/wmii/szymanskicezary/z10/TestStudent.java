package pl.edu.uwm.wmii.szymanskicezary.z10;

import pl.imiajd.szymanski.Student;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;

public class TestStudent {
    public static void main(String[] args) {
        ArrayList<Student> grupa = new ArrayList<>();
        grupa.add(new Student("Szymański", LocalDate.of(2000, 9, 11), 4.2));
        grupa.add(new Student("Baranowski", LocalDate.of(2000, 9, 11), 4.2));
        grupa.add(new Student("Baranowski", LocalDate.of(1999, 4, 5), 5.0));
        grupa.add(new Student("Malinowski", LocalDate.of(1998, 9, 11), 3.2));
        grupa.add(new Student("Boczek", LocalDate.of(1998, 9, 11), 4.8));
        for (Student e : grupa) {
            System.out.println(e.toString());
        }
        Collections.sort(grupa);
        System.out.println("Po posortowaniu:");
        for (Student e : grupa) {
            System.out.println(e.toString());
        }
    }
}
