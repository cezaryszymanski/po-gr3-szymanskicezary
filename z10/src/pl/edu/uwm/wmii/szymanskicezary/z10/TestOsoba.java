package pl.edu.uwm.wmii.szymanskicezary.z10;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import pl.imiajd.szymanski.Osoba;

public class TestOsoba {

    public static void main(String[] args) {
        ArrayList<Osoba>grupa=new ArrayList<>();
        grupa.add(new Osoba("Szymański",LocalDate.of(2000,9,11)));
        grupa.add(new Osoba("Baranowski",LocalDate.of(2000,9,11)));
        grupa.add(new Osoba("Baranowski",LocalDate.of(1999,4,5)));
        grupa.add(new Osoba("Malinowski",LocalDate.of(1998,9,11)));
        grupa.add(new Osoba("Boczek",LocalDate.of(1998,9,11)));
        for(Osoba e:grupa){
            System.out.println(e.toString());
        }
        Collections.sort(grupa);
        System.out.println("Po posortowaniu:");
        for(Osoba e:grupa){
            System.out.println(e.toString());
        }
    }
}