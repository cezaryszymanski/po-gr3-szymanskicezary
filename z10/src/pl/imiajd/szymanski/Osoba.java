package pl.imiajd.szymanski;

import java.time.LocalDate;

public class Osoba implements Cloneable, Comparable<Osoba>{
    private String nazwisko;
    private LocalDate dataUrodzenia;
    public Osoba(String nazwisko, LocalDate dataUrodzenia){
        this.nazwisko=nazwisko;
        this.dataUrodzenia=dataUrodzenia;
    }
    @Override
    public String toString(){
        return getClass().getSimpleName()+"["+nazwisko+" "+dataUrodzenia+"]";
    }
    public boolean equals(Osoba other){
        return nazwisko.equals(other.nazwisko)&&dataUrodzenia.equals(other.dataUrodzenia);
    }
    public int compareTo(Osoba other) {
        if (nazwisko.compareTo(other.nazwisko)>0){
            return 1;
        }
        if (nazwisko.compareTo(other.nazwisko)<0){
            return -1;
        }
        if (dataUrodzenia.compareTo(other.dataUrodzenia)>0){
            return 1;
        }
        if (dataUrodzenia.compareTo(other.dataUrodzenia)<0){
            return -1;
        }
        return 0;
    }
    @Override
    public Osoba clone() throws CloneNotSupportedException
    {
        Osoba cloned = (Osoba) super.clone();
        return cloned;
    }
}