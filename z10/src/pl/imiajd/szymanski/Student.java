package pl.imiajd.szymanski;

import java.time.LocalDate;

public class Student extends Osoba implements Cloneable,Comparable<Osoba>{
    private double sredniaOcen;
    public Student(String nazwisko,LocalDate dataUrodzenia,double sredniaOcen){
        super(nazwisko,dataUrodzenia);
        this.sredniaOcen=sredniaOcen;
    }
    public int compareTo(Student other){
        if (super.compareTo(other)==0){
            if(sredniaOcen>other.sredniaOcen){
                return 1;
            }
            if(sredniaOcen<other.sredniaOcen){
                return -1;
            }
            return 0;
        }
        return super.compareTo(other);
    }
    @Override
    public String toString(){
        return super.toString()+", "+sredniaOcen;
    }
}
