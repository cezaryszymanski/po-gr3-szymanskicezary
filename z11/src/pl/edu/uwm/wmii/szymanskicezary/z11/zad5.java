package pl.edu.uwm.wmii.szymanskicezary.z11;

import java.time.LocalDate;

public class zad5 {
    public static void main(String[] args){
        Integer[] intArr1={3,2,1};
        LocalDate data1=LocalDate.of(2000,1,1);
        LocalDate data2=LocalDate.of(2000,2,2);
        LocalDate data3=LocalDate.of(2000,3,3);
        LocalDate[] dateArr1={data3,data2,data1};
        System.out.println("Przed posortowaniem:");
        for(int i=0;i<intArr1.length;i++){
            System.out.println(intArr1[i]);
        }
        for(int i=0;i<dateArr1.length;i++){
            System.out.println(dateArr1[i]);
        }
        ArrayUtil.selectionSort(intArr1);
        ArrayUtil.selectionSort(dateArr1);
        System.out.println("Po posortowaniu:");
        for(int i=0;i<intArr1.length;i++){
            System.out.println(intArr1[i]);
        }
        for(int i=0;i<dateArr1.length;i++){
            System.out.println(dateArr1[i]);
        }
    }
}