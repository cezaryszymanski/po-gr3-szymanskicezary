package pl.edu.uwm.wmii.szymanskicezary.z11;

public class PairUtilDemo {
    public static void main(String[] args)
    {
        String[] words = { "Ala", "ma", "psa", "i", "kota" };
        Pair<String> mm = ArrayAlg.minmax(words);
        System.out.println("min = " + mm.getFirst());
        System.out.println("max = " + mm.getSecond());
        Pair<String>mm2=PairUtil.swap(mm);
        System.out.println("After swap:");
        System.out.println("min = " + mm2.getFirst());
        System.out.println("max = " + mm2.getSecond());
    }
}
class PairUtil{
    public static <T> Pair<T> swap(Pair<T> obj){
        Pair<T>temp=new Pair<>();
        temp.setFirst(obj.getFirst());
        temp.setSecond(obj.getSecond());
        temp.swap();
        return temp;
    }
}