package pl.edu.uwm.wmii.szymanskicezary.z11;

import java.time.LocalDate;

public class zad4 {
    public static void main(String[] args){
        Integer[] intArr1={1,2,3};
        LocalDate data1=LocalDate.of(2000,1,1);
        LocalDate data2=LocalDate.of(2000,2,2);
        LocalDate data3=LocalDate.of(2000,3,3);
        LocalDate[] dateArr1={data1,data2,data3};
        System.out.println(ArrayUtil.binSearch(intArr1,3));
        System.out.println(ArrayUtil.binSearch(intArr1,4));
        System.out.println(ArrayUtil.binSearch(dateArr1,data1));
        System.out.println(ArrayUtil.binSearch(dateArr1,LocalDate.of(2000,4,4)));
    }
}
