package pl.edu.uwm.wmii.szymanskicezary.z11;

import java.time.LocalDate;

public class zad3 {
    public static void main(String[] args){
        Integer[] intArr1={1,2,3};
        Integer[] intArr2={3,1,2};
        LocalDate data1=LocalDate.of(2000,1,1);
        LocalDate data2=LocalDate.of(2000,2,2);
        LocalDate data3=LocalDate.of(2000,3,3);
        LocalDate[] dateArr1={data1,data2,data3};
        LocalDate[] dateArr2={data3,data1,data2};
        System.out.println(ArrayUtil.isSorted(intArr1));
        System.out.println(ArrayUtil.isSorted(intArr2));
        System.out.println(ArrayUtil.isSorted(dateArr1));
        System.out.println(ArrayUtil.isSorted(dateArr2));
    }
}
class ArrayUtil{
    public static <T extends Comparable<T>> boolean isSorted(T[] a){
        for(int i=0;i< a.length-1;i++){
            if(a[i].compareTo(a[i+1])>0){
                return false;
            }
        }
        return true;
    }
    public static <T extends Comparable<T>> int binSearch(T[] a,T obj){
        int lewo=0;
        int prawo=a.length-1;
        int srodek=(lewo+prawo)/2;
        while(lewo<=prawo){
            if(a[srodek].compareTo(obj)==0){
                return srodek;
            }
            else if(a[srodek].compareTo(obj)<0) {
                lewo=srodek+1;
            }
            else{
                prawo=srodek-1;
            }
            srodek=(lewo+prawo)/2;
        }
        return -1;
    }
    public static <T extends Comparable<T>> void selectionSort(T[] a){
        int min;
        for(int i=0;i<a.length;i++){
            min=i;
            for(int j=i+1;j<a.length;j++){
                if(a[j].compareTo(a[min])<0){
                    min=j;
                }
                T temp=a[min];
                a[min]=a[i];
                a[i]=temp;
            }
        }
    }
}