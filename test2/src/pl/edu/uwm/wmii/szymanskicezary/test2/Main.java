package pl.edu.uwm.wmii.szymanskicezary.test2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class Main {

    public static void main(String[] args) {
        ArrayList<Kandydat> grupa=new ArrayList<>();
        grupa.add(new Kandydat("Karol",23,"licencjat",3));
        grupa.add(new Kandydat("Cezary",23,"mistrzowie",2));
        grupa.add(new Kandydat("Adrian",21,"mistrzowie",4));
        grupa.add(new Kandydat("Tomasz",21,"mistrzowie",1));
        for(Kandydat e:grupa){
            System.out.println(e.toString());
        }
        Collections.sort(grupa);
        System.out.println("Po posortowaniu:");
        for(Kandydat e:grupa){
            System.out.println(e.toString());
        }
        Rekrutacja.setDoswiadczenie();
        System.out.println("Z Qualified:");
        for(Kandydat e:grupa){
            System.out.println(e.toString()+", Qualified: "+Qualified(e));
        }
        System.out.println("Recriutment map:");
        HashMap<String,Integer> map = RecruitmentMap(grupa);
        for (String e:map.keySet()){
            System.out.println("nazwa: "+e+", lata doswiadczenia: "+map.get(e));
        }

    }
    public static boolean Qualified(Kandydat k){
        if (k.getLatadoswiadczenia()>=Rekrutacja.doswiadczenie){
            return true;
        }
        return false;
    }
    public static HashMap<String,Integer> RecruitmentMap(ArrayList<Kandydat> klist) {
        HashMap<String,Integer> mapa= new HashMap<>();
        for(Kandydat e :klist)
            if(Qualified(e))
                mapa.put(e.getNazwa(),e.getLatadoswiadczenia());
        return mapa;
    }
}
class Kandydat implements Cloneable, Comparable<Kandydat>{
    private String nazwa;
    private int wiek;
    private String wyksztalcony;
    private int latadoswiadczenia;
    public Kandydat(String nazwa,int wiek,String wyksztalcony,int latadoswiadczenia){
        this.nazwa=nazwa;
        this.wiek=wiek;
        this.wyksztalcony=wyksztalcony;
        this.latadoswiadczenia=latadoswiadczenia;
    }
    public String getNazwa(){
        return nazwa;
    }
    public int getWiek(){
        return wiek;
    }
    public String getWyksztalcony(){
        return wyksztalcony;
    }
    public int getLatadoswiadczenia(){
        return latadoswiadczenia;
    }
    public int compareTo(Kandydat other){
        if(wyksztalcony.compareTo(other.wyksztalcony)>0){
            return 1;
        }
        if(wyksztalcony.compareTo(other.wyksztalcony)<0){
            return -1;
        }
        if(wiek>other.wiek){
            return 1;
        }
        if(wiek<other.wiek){
            return -1;
        }
        if(latadoswiadczenia>other.latadoswiadczenia){
            return 1;
        }
        if(latadoswiadczenia<other.latadoswiadczenia){
            return -1;
        }
        return 0;
    }
    public String toString(){
        return "nazwa: "+nazwa+", "+"wiek: "+wiek+", "+"wyksztalcony: "+wyksztalcony+", "+"lata doswiadczenia: "+latadoswiadczenia;
    }
}
class Rekrutacja{
    public static int doswiadczenie;
    private ArrayList<Kandydat> lista;
    public static void setDoswiadczenie(){
        Rekrutacja.doswiadczenie=2;
    }
}
