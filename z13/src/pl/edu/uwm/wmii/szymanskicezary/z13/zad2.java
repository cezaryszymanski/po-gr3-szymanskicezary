package pl.edu.uwm.wmii.szymanskicezary.z13;

import java.util.*;

public class zad2 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        String polecenie="";
        HashMap<String,String> studenci=new HashMap<>();
        System.out.println("Polecenia:");
        System.out.println("zakoncz: zakoncz program");
        System.out.println("dodaj: dodaj studenta i ocene");
        System.out.println("usun: usun studenta");
        System.out.println("wyswietl: wyswietl studentow");
        while (!polecenie.equals("zakoncz")){
            System.out.print("Podaj polecenie: ");
            polecenie=sc.nextLine();
            if (polecenie.equals("dodaj")){
                System.out.println("Podaj nazwisko studenta: ");
                String nazwisko=sc.nextLine();
                System.out.println("Podaj ocene: ");
                String ocena=sc.nextLine();
                studenci.put(nazwisko,ocena);
            }
            if (polecenie.equals("usun")){
                System.out.println("Podaj nazwisko studenta do usuniecia: ");
                String nazwisko=sc.nextLine();
                studenci.remove(nazwisko);
            }
            if (polecenie.equals("zmien")){
                System.out.println("Podaj nazwisko studenta do zmiany oceny: ");
                String nazwisko=sc.nextLine();
                System.out.println("Podaj nowa ocene: ");
                String ocena=sc.nextLine();
                studenci.replace(nazwisko,ocena);
            }
            if (polecenie.equals("wyswietl")) {
                Map<String, String> treeMap = new TreeMap<>(studenci);
                for (String e : treeMap.keySet()) {
                    System.out.println(e + ": " + studenci.get(e));
                }
            }
        }
    }
}
