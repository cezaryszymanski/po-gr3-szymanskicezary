package pl.edu.uwm.wmii.szymanskicezary.z13;

import java.util.*;

public class zad3 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        String polecenie="";
        HashMap<Student,String> studenci=new HashMap<>();
        HashMap<Integer, Student> z_id= new HashMap<>();
        System.out.println("Polecenia:");
        System.out.println("zakoncz: zakoncz program");
        System.out.println("dodaj: dodaj studenta i ocene");
        System.out.println("usun: usun studenta");
        System.out.println("wyswietl: wyswietl studentow");
        while (!polecenie.equals("zakoncz")){
            System.out.print("Podaj polecenie: ");
            polecenie=sc.nextLine();
            if (polecenie.equals("dodaj")){
                System.out.println("Podaj imie studenta: ");
                String imie=sc.nextLine();
                System.out.println("Podaj nazwisko studenta: ");
                String nazwisko=sc.nextLine();
                System.out.println("Podaj ocene: ");
                String ocena=sc.nextLine();
                Student student=new Student(imie,nazwisko);
                studenci.put(student,ocena);
                z_id.put(student.getId(),student);
            }
            if (polecenie.equals("usun")){
                System.out.println("Podaj id studenta do usuniecia: ");
                int id=sc.nextInt();
                Student student=z_id.get(id);
                z_id.remove(id);
                studenci.remove(student);
            }
            if (polecenie.equals("zmien")){
                System.out.println("Podaj id studenta do zmiany oceny: ");
                int id = sc.nextInt();
                System.out.println("Podaj nowa ocene: ");
                String ocena=sc.nextLine();
                studenci.replace(z_id.get(id),ocena);
            }
            if (polecenie.equals("wyswietl")) {
                Collection kolekcja = z_id.values();
                ArrayList<Student> array = new ArrayList<>();
                for(Object e : kolekcja) {
                    array.add((Student) e);
                }
                array.sort(Student::compareTo);
                for(Student e : array) {
                    System.out.println(e.toString());
                }
            }
        }
    }
}
class Student implements Comparable<Student>{
    private String imie;
    private String nazwisko;
    private int id;
    private static int previous_id=0;
    public Student(String imie,String nazwisko) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.id = previous_id;
        previous_id++;
    }
    public int compareTo(Student other){
        if(imie.compareTo(other.imie)>0){
            return 1;
        }
        if(imie.compareTo(other.imie)<0){
            return -1;
        }
        if(nazwisko.compareTo(other.nazwisko)>0){
            return 1;
        }
        if(nazwisko.compareTo(other.nazwisko)<0){
            return -1;
        }
        if(id>other.id){
            return 1;
        }
        if(id<other.id){
            return -1;
        }
        return 0;
    }
    public String toString(){
        return nazwisko+", "+imie+", id:"+id;
    }
    public int getId() {
        return id;
    }
}
