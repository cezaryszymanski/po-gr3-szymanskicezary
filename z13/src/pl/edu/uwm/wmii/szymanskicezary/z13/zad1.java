package pl.edu.uwm.wmii.szymanskicezary.z13;

import java.util.Scanner;
import java.util.PriorityQueue;

public class zad1 {

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        String polecenie="";
        PriorityQueue<Zadanie> listaZadan=new PriorityQueue<>();
        while (!polecenie.equals("zakoncz")){
            System.out.print("Podaj polecenie: ");
            polecenie=sc.nextLine();
            if(polecenie.equals("dodaj priorytet opis")){
                System.out.println("Podaj priorytet: ");
                int priority= sc.nextInt();
                System.out.println("Podaj opis: ");
                String description= sc.next();
                listaZadan.add(new Zadanie(priority,description));
            }
            if(polecenie.equals("nastepne")){
                listaZadan.remove();
            }
            if(polecenie.equals("wyswietl")){
                for(Zadanie e: listaZadan){
                    System.out.println(e.toString());
                }
                if(listaZadan.isEmpty()){
                    System.out.println("Lista zadan jest pusta");
                }
            }
        }
    }
}

class Zadanie implements Comparable<Zadanie>{
    private int priority;
    private String description;
    public Zadanie(int priority, String description){
        this.priority=priority;
        this.description=description;
    }
    public int compareTo(Zadanie other){
        if(priority>other.priority){
            return 1;
        }
        if(priority<other.priority){
            return -1;
        }
        return 0;
    }
    public String toString(){
        return priority+"."+description;
    }
}