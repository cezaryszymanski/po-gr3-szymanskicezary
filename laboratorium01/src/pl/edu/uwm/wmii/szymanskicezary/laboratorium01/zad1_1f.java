package pl.edu.uwm.wmii.szymanskicezary.laboratorium01;

import java.util.Scanner;

public class zad1_1f {
    public static void main(String[] args) {
        Scanner myObj=new Scanner(System.in);
        System.out.println("Podaj n:");
        int n=myObj.nextInt();
        double dodawanie=0;
        double mnozenie=1;
        for (int i=1;i<=n;i++){
            System.out.println("Podaj "+i+" liczbe");
            double liczba=myObj.nextDouble();
            dodawanie+=liczba;
            mnozenie*=liczba;
        }
        System.out.println("Suma dodawania wynosi:"+dodawanie);
        System.out.println("Suma mnozenia wynosi:"+mnozenie);
    }
}
