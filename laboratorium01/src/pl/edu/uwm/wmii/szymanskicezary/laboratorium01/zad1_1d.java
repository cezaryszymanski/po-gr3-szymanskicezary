package pl.edu.uwm.wmii.szymanskicezary.laboratorium01;

import java.util.Scanner;
import java.lang.Math;
public class zad1_1d {
    public static void main(String[] args) {
        Scanner myObj=new Scanner(System.in);
        System.out.println("Podaj n:");
        int n=myObj.nextInt();
        double suma=0;
        for (int i=1;i<=n;i++){
            System.out.println("Podaj "+i+" liczbe:");
            double liczba=myObj.nextDouble();
            if(liczba<0){
                liczba*=(-1);
            }
            suma+=Math.sqrt(liczba);
        }
        System.out.println("Suma wynosi:"+suma);
    }
}
