package pl.edu.uwm.wmii.szymanskicezary.laboratorium05;

import java.util.Arrays;

public class zad2 {
    public static void main(String[] args) {
        IntegerSet s1=new IntegerSet();
        for (int i=1;i<100;i++){
            if (i<=4){
                s1.set[i]=true;
            }else{
                s1.set[i]=false;
            }
        }
        s1.writeSet(s1.set);
        System.out.println("");
        IntegerSet s2=new IntegerSet();
        for (int i=1;i<100;i++){
            if (i<=7){
                s2.set[i]=true;
            }else{
                s2.set[i]=false;
            }
        }
        s2.writeSet(s2.set);
        System.out.println("");
        IntegerSet.writeSet(IntegerSet.union(s1.set,s2.set));
        System.out.println("");
        IntegerSet.writeSet(IntegerSet.intersection(s1.set,s2.set));
        System.out.println("");
        s1.insertElement(44);
        s1.writeSet(s1.set);
        System.out.println("");
        s1.deleteElement(44);
        s1.writeSet(s1.set);
        System.out.println("");
        System.out.println(s1.toString());
        System.out.println("");
        System.out.println(s1.equals(s2));
    }
}
class IntegerSet{
    boolean[] set=new boolean[100];

    public static void writeSet(boolean[] set){
        for(int i=1;i<100;i++){
            if (set[i])
                System.out.println(i);
        }
    }
    public static boolean[] union(boolean[] s1, boolean[] s2){
        boolean[] us=new boolean[100];
        for (int i=1;i<100;i++){
            if(!s1[i]&&!s2[i]){
                us[i]=false;
            }else{
                us[i]=true;
            }
        }
        return us;
    }
    public static boolean[] intersection(boolean[] s1, boolean[] s2){
        boolean[] us=new boolean[100];
        for (int i=1;i<100;i++){
            if(s1[i]&&s2[i]){
                us[i]=true;
            }else{
                us[i]=false;
            }
        }
        return us;
    }
    public void insertElement(int liczba){
        set[liczba]=true;
    }
    public void deleteElement(int liczba){
        set[liczba]=false;
    }
    @Override
    public String toString(){
        String napis="";
        for(int i=1;i<100;i++){
            if (set[i]){
                napis+=(i+" ");
            }
        }
        return napis;
    }
    public boolean equals(IntegerSet set2){
        for(int i=1;i<100;i++){
            if(set[i]!=set2.set[i]){
                return false;
            }
        }
        return true;
    }
}
