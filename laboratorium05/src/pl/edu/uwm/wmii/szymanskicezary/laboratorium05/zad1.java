package pl.edu.uwm.wmii.szymanskicezary.laboratorium05;

public class zad1 {

    public static void main(String[] args) {
        RachunekBankowy saver1 = new RachunekBankowy(2000.00);
        RachunekBankowy saver2 = new RachunekBankowy(3000.00);
        System.out.println("Poczatkowe wartosci:");
        System.out.println(saver1.zwrocSaldo());
        System.out.println(saver2.zwrocSaldo());
        RachunekBankowy.setRocznaStopaProcentowa(0.04);
        saver1.obliczMiesieczneOdsetki();
        saver2.obliczMiesieczneOdsetki();
        System.out.println("Wartosci po 1 miesiacu:");
        System.out.println(saver1.zwrocSaldo());
        System.out.println(saver2.zwrocSaldo());
        RachunekBankowy.setRocznaStopaProcentowa(0.05);
        saver1.obliczMiesieczneOdsetki();
        saver2.obliczMiesieczneOdsetki();
        System.out.println("Wartosci po 2 miesiacu:");
        System.out.println(saver1.zwrocSaldo());
        System.out.println(saver2.zwrocSaldo());
    }
}
class RachunekBankowy{
    public static double rocznaStopaProcentowa;
    private double saldo;

    public RachunekBankowy(double start){
        saldo=start;
    }
    public void obliczMiesieczneOdsetki(){
        saldo+=(saldo*rocznaStopaProcentowa)/12;
    }
    public static void setRocznaStopaProcentowa(double wartosc){
        rocznaStopaProcentowa=wartosc;
    }
    public String zwrocSaldo() {
        return ""+saldo;
    }
}
