package pl.edu.uwm.wmii.szymanskicezary.laboratorium05;

import java.util.*;
import java.time.*;

public class zad3{

    public static void main(String[] args) {
        Pracownik[] personel = new Pracownik[3];

        personel[0] = new Pracownik("Karol Cracker", 75000, 2001, 12, 15);
        personel[1] = new Pracownik("Henryk Hacker", 50000, 2003, 10, 1);
        personel[2] = new Pracownik("Antoni Tester", 40000, 2005, 3, 15);

        for (Pracownik e : personel) {
            e.zwiekszPobory(20);
        }

        for (Pracownik e : personel) {
            System.out.print("nazwisko = " + e.nazwisko() + "\tpobory = " + e.pobory());
            System.out.printf("\tdataZatrudnienia = %tF\n", e.dataZatrudnienia());
        }
        System.out.println();

        LocalDate d = personel[0].dataZatrudnienia().minusYears(10);
        for (Pracownik e : personel) {
            System.out.print("nazwisko = " + e.nazwisko() + "\tpobory = " + e.pobory());
            System.out.printf("\tdataZatrudnienia = %tF\n", e.dataZatrudnienia());
        }
        System.out.println();
    }
}

class Pracownik {

    private String nazwisko;
    private double pobory;
    private LocalDate dataZatrudnienia;

    public Pracownik(String nazwisko, double pobory, int year, int month, int day) {
        this.nazwisko = nazwisko;
        this.pobory = pobory;

        GregorianCalendar calendar = new GregorianCalendar(year, month - 1, day);

        dataZatrudnienia = calendar.getTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }

    public String nazwisko() {
        return nazwisko;
    }

    public double pobory() {
        return pobory;
    }

    public LocalDate dataZatrudnienia() {
        return dataZatrudnienia;

    }

    public void zwiekszPobory(double procent) {
        double podwyżka = pobory * procent / 100;
        pobory += podwyżka;
    }
}