package pl.edu.uwm.wmii.szymanskicezary.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class zad1g {
    public static void main(String[] args) {
        int min=-999;
        int max=999;
        int n;
        int lewy;
        int prawy;
        Scanner scan=new Scanner(System.in);
        do{
            System.out.println("Podaj n <=1 i >=100:");
            n=scan.nextInt();
        }while(n<1||n>100);
        Random rand = new Random();
        int[] tab = new int[n];
        for(int i=0;i<n;i++){
            tab[i]=rand.nextInt((max+1)-min)+min;
            System.out.println(tab[i]);
        }

        do{
            System.out.println("Podaj lewy <=1 i >=n:");
            lewy=scan.nextInt();
        }while(lewy<1||lewy>=n);

        do{
            System.out.println("Podaj prawy <=1 i >=n:");
            prawy=scan.nextInt();
        }while(prawy<1||prawy>=n);
        if(lewy>prawy){
            int temp=lewy;
            lewy=prawy;
            prawy=temp;
        }
        while(lewy<prawy){
            int temp=tab[lewy];
            tab[lewy]=tab[prawy];
            tab[prawy]=temp;
            lewy++;
            prawy--;
        }
        System.out.println("PO ZMIANIE:");
        for(int i=0;i<n;i++){
            System.out.println(tab[i]);
        }

    }
}
