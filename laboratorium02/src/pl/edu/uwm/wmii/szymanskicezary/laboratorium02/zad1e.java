package pl.edu.uwm.wmii.szymanskicezary.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class zad1e {
    public static void main(String[] args) {
        int min=-999;
        int max=999;
        int n;
        int dodat=0;
        int najdluzszy=0;
        Scanner scan=new Scanner(System.in);
        do{
            System.out.println("Podaj n <=1 i >=100:");
            n=scan.nextInt();
        }while(n<1||n>100);
        Random rand = new Random();

        int[] tab = new int[n];
        for(int i=0;i<n;i++){
            tab[i]=rand.nextInt((max+1)-min)+min;
            System.out.println(tab[i]);
        }
        for(int i=0;i<n;i++){
            if(tab[i]>0){
                dodat++;
            }
            if(tab[i]<=0){
                if (dodat>najdluzszy){
                    najdluzszy=dodat;
                }
                dodat=0;
            }
        }
        System.out.println("Najdłuższy łańcuch liczb dodatnich:"+najdluzszy);
    }
}
