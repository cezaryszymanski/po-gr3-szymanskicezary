package pl.edu.uwm.wmii.szymanskicezary.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class zad3 {
    public static void main(String[] args) {
        int m;
        int n;
        int k;
        int min=1;
        int max=10;
        Scanner scan=new Scanner(System.in);
        do{
            System.out.println("Podaj m <=1 i >=10:");
            m=scan.nextInt();
        }while(m<1||m>10);
        do{
            System.out.println("Podaj n <=1 i >=10:");
            n=scan.nextInt();
        }while(n<1||n>10);
        do{
            System.out.println("Podaj k <=1 i >=10:");
            k=scan.nextInt();
        }while(k<1||k>10);

        int[][] a=new int[m][n];
        int[][] b=new int[n][k];
        System.out.println("a:");
        generate(a,m,n,min,max);
        System.out.println("b:");
        generate(b,n,k,min,max);
        int[][] c=mnozenie(a,b);
        System.out.println("c:");
        for(int i=0;i<c.length;i++){
            for (int j=0;j<c[0].length;j++){
                System.out.print(c[i][j]+" ");
            }
            System.out.println();
        }

    }

    public static void generate(int[][] tab, int n,int m, int min, int max){
        Random rand = new Random();
        for(int i=0;i<n;i++){
            for (int j=0;j<m;j++){
                tab[i][j]=rand.nextInt((max+1)-min)+min;
                System.out.print(tab[i][j]+" ");
            }
            System.out.println();
        }
    }
    public static int[][] mnozenie(int[][] a, int[][] b){
        int[][] c = new int[a.length][b[0].length];
        for(int i=0;i<a.length;i++){
            for(int j=0;j<b[0].length;j++){
                c[i][j]=0;
                for(int v=0;v<b.length;v++){
                    c[i][j]+=a[i][v]*b[v][j];
                }
            }
        }
        return c;
    }
}
