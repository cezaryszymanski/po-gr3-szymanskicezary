package pl.edu.uwm.wmii.szymanskicezary.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class zad2c {
    public static void main(String[] args) {
        int min=-999;
        int max=999;
        int n;
        Scanner scan=new Scanner(System.in);
        do{
            System.out.println("Podaj n <=1 i >=100:");
            n=scan.nextInt();
        }while(n<1||n>100);
        int[] tab = new int[n];
        generate(tab,n,min,max);
        System.out.println("Najwiekszy element wystepuje "+ileMaksymalnych(tab)+" razy");
    }

    public static void generate(int[] tab, int n, int min, int max){
        Random rand = new Random();
        for(int i=0;i<n;i++){
            tab[i]=rand.nextInt((max+1)-min)+min;
            System.out.println(tab[i]);
        }
    }
    public static int ileMaksymalnych(int[] tab){
        int najwiekszy=tab[0];
        int ile=0;
        for(int i:tab){
            if (i>najwiekszy){
                najwiekszy=i;
            }
        }
        for(int i:tab){
            if (i==najwiekszy){
                ile++;
            }
        }
        return ile;
    }
}
