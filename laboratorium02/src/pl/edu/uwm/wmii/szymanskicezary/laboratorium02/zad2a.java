package pl.edu.uwm.wmii.szymanskicezary.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class zad2a {
    public static void main(String[] args) {
        int min=-999;
        int max=999;
        int n;
        Scanner scan=new Scanner(System.in);
        do{
            System.out.println("Podaj n <=1 i >=100:");
            n=scan.nextInt();
        }while(n<1||n>100);
        int[] tab = new int[n];
        generate(tab,n,min,max);
        System.out.println("Parzyste:"+ileParzystych(tab)+", Nieparzyste:"+ileNieparzystych(tab));
    }

    public static void generate(int[] tab, int n, int min, int max){
        Random rand = new Random();
        for(int i=0;i<n;i++){
            tab[i]=rand.nextInt((max+1)-min)+min;
            System.out.println(tab[i]);
        }
    }
    public static int ileNieparzystych(int[] tab){
        int nieparzyste=0;
        for(int i:tab){
            if (i%2!=0){
                nieparzyste++;
            }
        }
        return nieparzyste;
    }
    public static int ileParzystych(int[] tab){
        int parzyste=0;
        for(int i:tab){
            if (i%2==0){
                parzyste++;
            }
        }
        return parzyste;
    }
}
