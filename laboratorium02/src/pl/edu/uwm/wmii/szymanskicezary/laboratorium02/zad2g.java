package pl.edu.uwm.wmii.szymanskicezary.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class zad2g {
    public static void main(String[] args) {
        int min=-999;
        int max=999;
        int n;
        int lewy;
        int prawy;
        Scanner scan=new Scanner(System.in);
        do{
            System.out.println("Podaj n <=1 i >=100:");
            n=scan.nextInt();
        }while(n<1||n>100);
        int[] tab = new int[n];
        generate(tab,n,min,max);
        do{
            System.out.println("Podaj lewy <=1 i >=n:");
            lewy=scan.nextInt();
        }while(lewy<1||lewy>=n);

        do{
            System.out.println("Podaj prawy <=1 i >=n:");
            prawy=scan.nextInt();
        }while(prawy<1||prawy>=n);
        odwrocFragment(tab,lewy,prawy);
        System.out.println("PO ZMIANIE:");
        for(int i=0;i<n;i++){
            System.out.println(tab[i]);
        }
    }

    public static void generate(int[] tab, int n, int min, int max){
        Random rand = new Random();
        for(int i=0;i<n;i++){
            tab[i]=rand.nextInt((max+1)-min)+min;
            System.out.println(tab[i]);
        }
    }
    public static void odwrocFragment(int[] tab, int lewy, int prawy){
        if(lewy>prawy){
            int temp=lewy;
            lewy=prawy;
            prawy=temp;
        }
        while(lewy<prawy){
            int temp=tab[lewy];
            tab[lewy]=tab[prawy];
            tab[prawy]=temp;
            lewy++;
            prawy--;
        }
    }
}
