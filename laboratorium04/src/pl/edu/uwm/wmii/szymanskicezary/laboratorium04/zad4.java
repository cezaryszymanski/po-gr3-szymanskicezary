package pl.edu.uwm.wmii.szymanskicezary.laboratorium04;

import java.util.ArrayList;

public class zad4{
    public static void main(String[] args) {
        ArrayList<Integer> arr1=new ArrayList<>(8);
        for(int i=0;i<8;i++){
            arr1.add(i,i+1);
        }
        System.out.println(arr1);
        System.out.println(reversed(arr1));
    }
    public static ArrayList<Integer> reversed(ArrayList<Integer> a){
        ArrayList<Integer> c=new ArrayList<>();
        for(int i=a.size();i>0;i--){
            c.add(a.get(i-1));
        }
        return c;
    }
}
