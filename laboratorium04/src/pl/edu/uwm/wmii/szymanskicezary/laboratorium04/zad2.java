package pl.edu.uwm.wmii.szymanskicezary.laboratorium04;

import java.util.ArrayList;

public class zad2 {
    public static void main(String[] args) {
        ArrayList<Integer> arr1=new ArrayList<>(8);
        for(int i=0;i<8;i++){
            arr1.add(i,i+1);
        }
        System.out.println(arr1);
        ArrayList<Integer> arr2=new ArrayList<>(3);
        for(int i=0;i<3;i++){
            arr2.add(i,arr2.size()+i+1);
        }
        System.out.println(arr2);
        System.out.println(merge(arr1,arr2));
    }
    public static ArrayList<Integer> merge(ArrayList<Integer> a, ArrayList<Integer> b){
        ArrayList<Integer> c=new ArrayList<>();
        if(a.size()>b.size()){
            for (int i=0;i<a.size();i++){
                c.add(a.get(i));
                if(i<b.size()){
                    c.add(b.get(i));
                }
            }
        }else{
            for (int i=0;i<b.size();i++){
                if(i<a.size()){
                    c.add(a.get(i));
                }
                c.add(b.get(i));
            }
        }
        return c;
    }
}
