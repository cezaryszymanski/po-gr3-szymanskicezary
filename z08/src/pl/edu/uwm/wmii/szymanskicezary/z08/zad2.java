package pl.edu.uwm.wmii.szymanskicezary.z08;

import pl.imiajd.szymanski.Osoba;
import pl.imiajd.szymanski.Pracownik;
import pl.imiajd.szymanski.Student;

import java.time.LocalDate;

public class zad2 {
    public static void main(String[] args)
    {
        String[] imiona_1={"Marcin","Łukasz"};
        String[] imiona_2={"Karolina","Dorota"};
        String[] imiona_3={"Cezary","Mateusz"};
        Osoba[] ludzie = new Osoba[3];
        ludzie[0] = new Pracownik("Kowalski",imiona_1, LocalDate.of(1976,9,20),true,5000,LocalDate.of(2010,8,1));
        ludzie[1] = new Student("Nowak",imiona_2,LocalDate.of(2000,4,11),false,"ekonomia",3.4);
        Student student1=new Student("Szymański",imiona_3,LocalDate.of(2000,4,11),true,"informatyka",4.2);
        student1.setŚredniaOcen(3.9);
        ludzie[2]=student1;

        for (Osoba p : ludzie) {
            System.out.println(p.getImiona()+", "+p.getNazwisko()+", data urodzenia:"+p.getDataUrodzenia()+", płeć:"+p.getPlec()+": " + p.getOpis());
        }
    }
}
