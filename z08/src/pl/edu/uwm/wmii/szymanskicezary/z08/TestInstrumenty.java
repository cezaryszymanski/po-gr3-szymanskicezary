package pl.edu.uwm.wmii.szymanskicezary.z08;

import java.time.LocalDate;
import java.util.ArrayList;
import pl.imiajd.szymanski.Instrument;
import pl.imiajd.szymanski.Flet;
import pl.imiajd.szymanski.Fortepian;
import pl.imiajd.szymanski.Skrzypce;

public class TestInstrumenty {
    public static void main(String[] args){
        ArrayList<Instrument>orkiestra=new ArrayList(5);
        orkiestra.add(new Flet("Yamacha",LocalDate.of(2010,6,7)));
        orkiestra.add(new Flet("Yamacha",LocalDate.of(2010,6,7)));
        orkiestra.add(new Flet("Philips",LocalDate.of(2010,6,7)));
        orkiestra.add(new Fortepian("Bosendorfer",LocalDate.of(2000,1,4)));
        orkiestra.add(new Skrzypce("Stradivarius",LocalDate.of(2005,11,22)));
        for(Instrument i:orkiestra){
            System.out.println(i.toString());
        }
        System.out.println(orkiestra.get(0).equals(orkiestra.get(1)));
        System.out.println(orkiestra.get(0).equals(orkiestra.get(2)));
    }
}
