package pl.edu.uwm.wmii.szymanskicezary.z08;

import java.time.LocalDate;
import java.util.Arrays;

public class zad1{

    public static void main(String[] args)
    {
        String[] imiona_1={"Marcin","Łukasz"};
        String[] imiona_2={"Karolina","Dorota"};
        String[] imiona_3={"Cezary","Mateusz"};
        Osoba[] ludzie = new Osoba[3];
        ludzie[0] = new Pracownik("Kowalski",imiona_1,LocalDate.of(1976,9,20),true,5000,LocalDate.of(2010,8,1));
        ludzie[1] = new Student("Nowak",imiona_2,LocalDate.of(2000,4,11),false,"ekonomia",3.4);
        Student student1=new Student("Szymański",imiona_3,LocalDate.of(2000,4,11),true,"informatyka",4.2);
        student1.setŚredniaOcen(3.9);
        ludzie[2]=student1;

        for (Osoba p : ludzie) {
            System.out.println(p.getImiona()+", "+p.getNazwisko()+", data urodzenia:"+p.getDataUrodzenia()+", płeć:"+p.getPlec()+": " + p.getOpis());
        }
    }
}

abstract class Osoba
{
    public Osoba(String nazwisko, String[] imiona, LocalDate dataUrodzenia,boolean plec) {
        this.nazwisko = nazwisko;
        this.imiona = imiona;
        this.dataUrodzenia = dataUrodzenia;
        this.plec = plec;
    }

    public abstract String getOpis();

    public String getNazwisko()
    {
        return nazwisko;
    }
    public String getImiona() {
        return Arrays.toString(imiona).replace("[", "").replace("]", "");
    }
    public LocalDate getDataUrodzenia() {
        return dataUrodzenia;
    }
    public String getPlec(){
        if(plec){
            return "mężczyzna";
        }
        return "kobieta";
    }
    private String nazwisko;
    private String[] imiona;
    private LocalDate dataUrodzenia;
    private boolean plec;

}

class Pracownik extends Osoba
{
    public Pracownik(String nazwisko,String[] imiona, LocalDate dataUrodzenia,boolean plec, double pobory,LocalDate dataZatrudnienia)
    {
        super(nazwisko, imiona, dataUrodzenia, plec);
        this.pobory = pobory;
        this.dataZatrudnienia=dataZatrudnienia;
    }

    public double getPobory()
    {
        return pobory;
    }
    public LocalDate getDataZatrudnienia(){
        return dataZatrudnienia;
    }
    @Override
    public String getOpis()
    {
        return String.format("pracownik z pensją %.2f zł", getPobory())+", data zatrudnienia:"+getDataZatrudnienia();
    }
    private double pobory;
    private LocalDate dataZatrudnienia;

}


class Student extends Osoba
{
    public Student(String nazwisko,String[] imiona, LocalDate dataUrodzenia,boolean plec, String kierunek,double średniaOcen)
    {
        super(nazwisko, imiona, dataUrodzenia, plec);
        this.kierunek = kierunek;
        this.średniaOcen=średniaOcen;
    }

    public double getŚredniaOcen() {
        return średniaOcen;
    }
    public void setŚredniaOcen(double srednia){
        średniaOcen=srednia;
    }
    @Override
    public String getOpis()
    {
        return "kierunek studiów:" + kierunek+", średnia ocen:"+getŚredniaOcen();
    }
    private String kierunek;
    private double średniaOcen;

}
