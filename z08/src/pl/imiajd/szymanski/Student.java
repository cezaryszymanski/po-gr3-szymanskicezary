package pl.imiajd.szymanski;

import java.time.LocalDate;

public class Student extends Osoba
{
    public Student(String nazwisko, String[] imiona, LocalDate dataUrodzenia, boolean plec, String kierunek, double średniaOcen)
    {
        super(nazwisko, imiona, dataUrodzenia, plec);
        this.kierunek = kierunek;
        this.średniaOcen=średniaOcen;
    }

    public double getŚredniaOcen() {
        return średniaOcen;
    }
    public void setŚredniaOcen(double srednia){
        średniaOcen=srednia;
    }
    @Override
    public String getOpis()
    {
        return "kierunek studiów:" + kierunek+", średnia ocen:"+getŚredniaOcen();
    }
    private String kierunek;
    private double średniaOcen;

}
