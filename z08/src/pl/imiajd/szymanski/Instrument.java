package pl.imiajd.szymanski;

import java.time.LocalDate;

public abstract class Instrument{
    private String producent;
    private LocalDate rokProdukcji;
    public Instrument(String producent,LocalDate rokProdukcji){
        this.producent=producent;
        this.rokProdukcji=rokProdukcji;
    }
    public String getProducent() {
        return producent;
    }
    public LocalDate getRokProdukcji() {
        return rokProdukcji;
    }
    public abstract String dzwiek();
    @Override
    public String toString(){
        return "producent:"+producent+", rok produkcji:"+rokProdukcji+", dzwiek:"+dzwiek();
    }
    public boolean equals(Instrument instr){
        return producent.equals(instr.producent)&&rokProdukcji.equals(instr.rokProdukcji)&&dzwiek().equals(instr.dzwiek());
    }
}
