package pl.imiajd.szymanski;

import java.time.LocalDate;
import java.util.Arrays;

public abstract class Osoba
{
    public Osoba(String nazwisko, String[] imiona, LocalDate dataUrodzenia, boolean plec) {
        this.nazwisko = nazwisko;
        this.imiona = imiona;
        this.dataUrodzenia = dataUrodzenia;
        this.plec = plec;
    }

    public abstract String getOpis();

    public String getNazwisko()
    {
        return nazwisko;
    }
    public String getImiona() {
        return Arrays.toString(imiona).replace("[", "").replace("]", "");
    }
    public LocalDate getDataUrodzenia() {
        return dataUrodzenia;
    }
    public String getPlec(){
        if(plec){
            return "mężczyzna";
        }
        return "kobieta";
    }
    private String nazwisko;
    private String[] imiona;
    private LocalDate dataUrodzenia;
    private boolean plec;

}
